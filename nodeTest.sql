CREATE TABLE test (
    id SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
    text TEXT,
    insert_date DATETIME NOT NULL,
    PRIMARY KEY(id)
);

CREATE TABLE users (
    id SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
    email VARCHAR(255),
    lastname VARCHAR(255),
    firstname VARCHAR(255),
    postalcode VARCHAR(10),
    phone VARCHAR(10),
    gender VARCHAR(10),
    userbirth VARCHAR(10),
    insertDate DATETIME NOT NULL,
    PRIMARY KEY(id)
);
CREATE TABLE connectionCodes (
    id SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
    phone VARCHAR(10),
    code VARCHAR(6),
    PRIMARY KEY(id)
);

CREATE TABLE criteria (
    id SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
    criterionType VARCHAR(8),
    criterionName VARCHAR(255),
    criterionValueType VARCHAR(8),
    criterionAdmissibleValues VARCHAR(255),
    PRIMARY KEY(id)
);
CREATE TABLE criteriaAllAdmissibleValues (
    id SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
    value VARCHAR(255),
    PRIMARY KEY(id)
);
CREATE TABLE comments (
    id SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
    idUser SMALLINT UNSIGNED NOT NULL,
    comment TEXT,
    insertDate DATETIME NOT NULL,
    PRIMARY KEY(id)
);
CREATE TABLE diags (
    id SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
    phoneUser VARCHAR(10),
    lat1 FLOAT,
    lon1 FLOAT,
    lat2 FLOAT,
    lon2 FLOAT,
    PRIMARY KEY(id)
);
CREATE TABLE trips (
    id SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
    phoneUser VARCHAR(10),
    lat1 FLOAT,
    lon1 FLOAT,
    lat2 FLOAT,
    lon2 FLOAT,
    mode VARCHAR(50),
    departure VARCHAR(500),
    departureHour VARCHAR(10),
    arrival VARCHAR(500),
    arrivalHour VARCHAR(10),
    AR SMALLINT UNSIGNED,
    PRIMARY KEY(id)
);

CREATE TABLE errors (
    id SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
    value TEXT,
    PRIMARY KEY(id)
);