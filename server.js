/*
	Chargement des modules MySQL et NodeJS nécessaires pour la connexion aux bases de données de Wever.
*/

var mysql = require('mysql');
var MongoClient = require("mongodb").MongoClient;

//  Chargement du micro-framework Express.
var express = require('express');

/*
	Chargement des composants express classiques pour gérer le routage, les requêtes GET et POST,
	les sessions et le cryptage des mots de passe des utilisateurs stockés dans la base de données MySQL.
*/

var url = require('url');
var querystring = require('querystring');
var bodyParser = require('body-parser');
var urlencodedParser = bodyParser.urlencoded({extended : false});
var session = require('cookie-session');
var crypto = require('crypto');
var favicon = require('serve-favicon');

// Un module pour transformer du json en csv
var json2csv = require('json2csv');


/*
	On charge le module contenant les fonctions utiles pour les diagnostiques.
	Il faudra par contre penser ce module comme une classe.
	Il dispose d'attributs qui sont tous privés (principe d'encapsulation) et
	certaines de ses fonctions internes sont publiques et accessibles depuis
	l'extérieur.
*/
var diags = require('./diags');

// Rayon maximal en km pour lequel on pourra afficher des résultats de diagnostiques atour d'un point.
var m_maxRadius = 2;

/*
	Les attributs globaux
*/
var weverDB = null;

/*
	On gère deux bases de données (une en MySQl et l'autre en NODE JS) car à mon arrivée, une base de données (en MongoDB) des utilisateurs
	du site existait. Il fallait donc gérer ces anciens utilisateurs sur la nouvelle plateforme que nous allions construire.
*/

//Les accès aux bases de données MySQL et MongoDB.

var settings = {
  mongo : {
    host: process.env.MONGO_HOST || 'db',
    port : process.env.MONGO_PORT || 27017,
    database: process.env.MONGO_DB || 'wever',
    user: process.env.MONGO_USER,
    password: process.env.MONGO_PASSWD
  },
  mysql: {
    host: process.env.MYSQL_HOST || 'db',
    port : process.env.MYSQL_PORT || 3306,
    database: process.env.MYSQL_DB || 'diags',
    user: process.env.MYSQL_USER,
    password: process.env.MYSQL_PASSWD
  },
  allmysms: {
    endpoint: "https://api.allmysms.com/http/9.0?",
    credentials: {
      login: 'wever',
      apiKey: '7dc89d06afba3e0'
    }
  },
  yolo: {
    url: process.env.YOLO_URL
  },
  admin: {
    phone: process.env.ADMIN_MOBILE || "0625613079"
  },
  server: {
    mode: process.env.SERVER_MODE || 'standalone'
  }
}


var mongoDSN = 'mongodb://' + settings.mongo.user + ':' + settings.mongo.password + '@' + settings.mongo.host +':' + settings.mongo.port + '/' + settings.mongo.database;

/*
	Connexion à la base de données Mongodb.
*/
MongoClient.connect(mongoDSN, function(error, db) {
    if (!error)
    {
		weverDB = db;
	}
	else
	{
		console.log(error);
	}
});

/*
	Connexion à la base de données MySQL de notre site web.
*/
var connection = mysql.createConnection(settings.mysql);

connection.connect(function(err){
	if(err)
	{
		console.log("Error when connecting to the database.");
		console.log(err);
	}
});

/*
	Fin de la connexion à la base de données MySQL de notre site web.
*/


// Objet request pour effectuer des requêtes sur d'autres domaines.
var request = require('request');

// lodash est une librairie node js ayant beaucoup de fonctions utiles.
// En particulier, on peut l'utiliser pour concaténer deux objets JSON
// entre eux.
var _ = require('lodash');

// Nombre de locations/communautés maximum que l'on peut récupérer
var maxLocations = 5;

// Normalement, il ne doit y avoir que l'attribut connection
// pour permettre au module diags d'avoir accès à la connexion présente.
// Mais, il est utile dans le module diags de connaître explicitement le nom de la base de données(pour créer des diagnostiques notamment).
// Donc on passe cette valeur en second paramètre.
diags.getConnectionPointer(connection,settings.mysql.database);

// Ici on récupère de la base de données toutes les valeurs possibles de critères discrets.
// Cette liste sera ainsi disponible pour tous les utilisateurs qui se connecteront sur ce serveur Node JS.
// Cette liste est évidemment mise à jour en interne dès que l'utilisateur effectue une des opérations
// CRUD(Create,Retrieve,Update,Delete) sur la liste des valeurs possibles de critères de type discret.
diags.retrieveAllCriteriaAdmissibleValues();

// Idem mais pour les critères présents dans la base de données.
diags.retrieveCriteria();

var app = express();

app
.use(session({secret: 'abcdefghijklmnopqrstuvwxyz'})) // On active les sessions (que l' on sécurise avec un mot de passe).
.use(express.static('public')) // On déclare public ( qui contient les images ,les polices,etc...) pour pouvoir accéder aux images , polices , etc...

.get("/",function(req,res){
	var queries = querystring.parse(url.parse(req.url).query);

	/***********************************************************************************************************************/
	/****************** Authentification des utilisateurs provenant de l'appli Wever(Debut) ********************************/
	/***********************************************************************************************************************/

	// On vérifie avant si un token est défini dans l'URL. Si oui cela veut dire qu'un utilisateur a cliqué sur le bouton diagnostic
	// lors de la déclaration de son trajet. Il faut le diriger directement vers le diagnostic sans qu'il n'ait besoin de s'authentifier
	// ou de déclarer à nouveau un trajet.

	if(typeof(queries['access_token']) != 'undefined')
	{
		request.get(settings.yolo.url+'/user/me?access_token='+queries['access_token'], function(err,httpResponse,result){
			result = JSON.parse(result);
			if(typeof(result.lastname) != 'undefined') 
			{

				var usersex = "";

				if(result.sex == "femme")
				{
					usersex = "female";
				}
				if(result.sex == "homme")
				{
					usersex = "male";
				}

				var postalCode = null;

				if(typeof(result.adresses) != 'undefined' && result.adresses[0].postalCode)
				{
					postalCode = result.adresses[0].postalCode;
				}

				req.session.user = {"lastname" : result.lastname,"firstname" : result.firstname,"postalcode" : postalCode,
					"phone" : "0"+result.phone.slice(3),"gender":usersex,"birth":result.dob,"email" : result.email};
				res.redirect("/");
			}
			else
			{
				res.redirect("/");
			}
		});
		/**********************************************************************************************************************/
		/****************** Authentification des utilisateurs provenant de l'appli Wever (Fin) ********************************/
		/**********************************************************************************************************************/
	}
	else
	{

	// Si aucune session n'est définie ou que la session est vide, l' utilisateur est dirigé vers la page d' authentification.
	// Si une session existe et est non vide ,mais que l' utilisateur n'a pas l'autorisation d'accéder à la page d'accueil (oui ce cas critique
	// est possible non pas parce que la déconnexion est mal gérée mais parce que la connexion par numéro de téléphone s'effectue en deux temps-
	// si l'utilisateur entre un bon numéro de téléphone et un mauvais code secret, il ne doit pas pouvoir accéder à la page d'accueil du site).
	if (typeof(req.session.user) == 'undefined' || req.session['user'] == null || ( typeof(req.session.notAllowedYet) != 'undefined' && req.session.notAllowedYet == true ) ) {
		if(typeof(queries['process']) != 'undefined' && queries['process'] == "register")
		{
			res.render("register.ejs");
		}
		else
		{
			res.render("connection.ejs");
		}
	}
	else // Dans le cas contraire, l' utilisateur est dirigé vers la page d'accueil du site.
	{
		// On récupère les critères et la liste des valeurs possibles des critères de type discret préalablement stockés 
		// dans des attributs privés du module diags (pensez-y toujours comme une classe) dès le démarrage du serveur.
		// Ces attributs sont détruits dès l'arrêt du serveur et recréés au démarrage.
		// Ils sont un peu comme une sorte de cache, de mémoire vive.
		// Ils permettent aussi l'accès rapide à ces données critiques sans aller dans la base de données.
		ctx = {};
		ctx["allCriteriaAdmissibleValues"] = diags.getAllCriteriaAdmissibleValues();
		ctx["allCriteria"] = diags.getCriteria();

		// On récupère la session de l'utilisateur
		ctx["user"] = req.session['user'];
		ctx["admin"] = false;
		if(req.session["user"].phone == settings.admin.phone)
		{
			ctx["admin"] = true;
		}
		//res.render("diagsDemo.ejs",ctx);
		res.render("diag.ejs",ctx);
	}
	}
})

// l'url /register en méthode POST gère l' inscription.
.post("/register",urlencodedParser,function(req,res){
	
	var userphone = req.body.userphone;

	weverDB.collection("users").findOne({phone : "+33"+userphone.slice(1)}, function(error, result) {
	    if (error) console.log(error);

		if(result != null) // Si présent dans la base de données de Wever... On le redirige vers la page de connexion avec un booléen disant que le système l'a reconnu.
		{
			res.redirect("/");
		}
		else
		{
			var firstname = req.body.firstname;
			var lastname = req.body.lastname;
			var usermail = req.body.usermail;
			var userpostal = req.body.userpostal;
			var usergender = req.body.gender;
			var userbirth = req.body.userbirthdate;

			var userbirthDay = userbirth.substring(0,2);
			var userbirthMonth = userbirth.substring(3,5);
			var userbirthYear = userbirth.substring(6,10);

			var userbirthWever = userbirthYear + '-' + userbirthMonth + '-' + userbirthDay;

			/*var sql2 = 'SELECT phone FROM users WHERE phone='+'"'+userphone+'"';

			connection.query(sql2,function(err,row){
				if(!err && row.length == 0) // Si l'utilisateur n' est pas encore dans notre base de données, on l'insère...
				{
					// Dans la base de données MySQL...
					// Want to change this using a prepared request but node js mysql module does not allow prepared requests !!
					var sql3 = 'INSERT INTO users(lastname,firstname,postalcode,phone,gender,userbirth,email,insertDate) VALUES('+'"'+lastname
						+'"'+","+'"'+firstname+'"'+","+'"'+userpostal+'"'+","+'"'+userphone+'"'+","+'"'+usergender+'"'+","+'"'
						+userbirth+'"'+","+'"'+usermail+'"'+",NOW()"+')';

					connection.query(sql3,function(err){
						if(err)
						{
							console.log(err);
						}
						else // ... on stocke les informations le concernant dans la session courante.
						{
							req.session.user = {"lastname" : lastname,"firstname" : firstname,"postalcode" : userpostal,"phone" : userphone,
							"gender":usergender,"birth":userbirth,"email" : usermail};
							res.redirect("/"); // ... et on le redirige vers la page d' accueil.
						}
					});*/

					// ... et dans la base de données de WEVER

						req.session.user = {"lastname" : lastname,"firstname" : firstname,"postalcode" : userpostal,"phone" : userphone,
							"gender":usergender,"birth":userbirth,"email" : usermail};

						var usersex = "";

						if(usergender == "female")
						{
							usersex = "femme";
						}
						if(usergender == "male")
						{
							usersex = "homme";
						}

					    var user = {
										phone : "+33"+userphone.slice(1),
										groups : [ ],
										roles : [       "user",       "cguv2",       "vinci",       "did"     ],
										avatars : [],
										credentials : {},
										addresses : [       {         "postalCode" : userpostal,       }     ],
										email : usermail,
										firstname : firstname.charAt(0).toUpperCase() + firstname.slice(1),
										lastname : lastname.toUpperCase(),
										sex : usersex,
										dob : new Date(userbirthWever+"T23:00:00Z"),
										diagUser : "true"
									};
						weverDB.collection("users").insert(user, null, function (error, results) {
							if(!error)
							{
								console.log("Utilisateur insere avec success dans la base de donnees MongoDB de Wever.");
								res.redirect("/");
							}
							else
							{
								console.log(error);
							}
						});

				/*}
				else if(!err && row.length > 0) // Sinon...
				{
					// on le redirige vers la page d' accueil avec un booléen qui permettra d' afficher un message lui demandant de se connecter
					// plutôt que d' eessayer de se réinscrire.
					res.redirect("/");
				}
			});*/
		}
	});

})

.get("/verifyCode",function(req,res){
	var phone = querystring.parse(url.parse(req.url).query)["phone"];
	var code = querystring.parse(url.parse(req.url).query)["code"];
	
	var sql = 'SELECT * FROM connectionCodes WHERE phone='+'"'+phone+'" AND code='+'"'+code+'"';
	connection.query(sql,function(err,row){
		if(!err && row.length > 0)
		{
			res.send("1");
		}
		else
		{
			res.send("0");
		}
	});
})

// l'url /connection en méthode POST gère la connexion.
.post("/connection",urlencodedParser,function(req,res){
	var connectionUserphone = req.body.connectionUserphone;

	// Sera utilisé plus tard pour gérer la connexion par code envoyé via SMS. Il faudra créer une table dans la base de données avec les numéros
	// et les codes de connexion correspondants. Dès que le code sera vérifié , la ligne correspondante dans la base de données sera détruite.
	var connectionCode = req.body.connectionCode;

	var sql = 'SELECT * FROM connectionCodes WHERE phone='+'"'+connectionUserphone+'" AND code='+'"'+connectionCode+'"';

	connection.query(sql,function(err,row){
		if(!err && row.length > 0) // Si les informations de connexion sont correctes...
		{
			req.session.notAllowedYet = false;
			var sql3 = 'DELETE FROM connectionCodes WHERE phone='+'"'+connectionUserphone+'"';
			connection.query(sql3,function(err){
				console.log(err);
			});
			/*if(!req.session.nativeWeverUser)
			{
				var sql2 = 'SELECT * FROM users WHERE phone='+'"'+connectionUserphone+'"'; // On récupère les informations de l'utilisateur...
				connection.query(sql2,function(err,row){
					// ... on stocke les informations concernant l'utilisateur dans la session courante.
					req.session.user = {"lastname" : row[0].lastname,"firstname" : row[0].firstname,"postalcode" : row[0].postalcode,"phone" : row[0].phone,
						"gender":row[0].gender,"birth":row[0].userbirth,"email" : row[0].email};
					res.redirect("/"); // Et on le redirige vers la page d'accueil.
				});
			}
			else
			{
				res.redirect("/");
			}*/
			res.redirect("/");
		}
		else if(!err && row.length == 0) // Sinon...
		{
			// On le redirige tout simplement vers la page d' accueil sans remplir sa session ou en la vidant si elle existe.
			if(typeof(req.session.user) != 'undefined')
			{
				req.session["user"] = null;
			}
			res.redirect("/");
		}
	});
})

// Cette url permet de vérifier qu'un numéro de téléphone est bien présent dans nos bases de donnée MySQL ou NodeJS.
// Si oui on crée un code que l'on envoie au client par SMS d'un côté et que l'on sauvegarde temporairement dans notre base de données de l'autre.
// Si non on renvoie unknown pour signifier que le système ne reconnaît pas ce numéro de téléphone.
.get("/verifyPhoneAndCreateCode",function(req,res){
	var phone = querystring.parse(url.parse(req.url).query)["phone"];

	/*
		On vérifie si le numéro de téléphone entré est un numéro de téléphone d' un utilisateur natif de Wever (pas un utilisateur
		Wever préalablement créé à partir du diagnostic).
	*/

	if(typeof(phone) != 'undefined')
	{

		weverDB.collection("users").findOne({phone : "+33"+phone.slice(1)}, function(error, result) {
		    if (error) console.log(error);

		    //if(result != null && result.diagUser != "true") // Si utilisateur natif de Wever... 

			if(result != null) // Si utilisateur de Wever...
			{
				// On remplit la session de l'utilisateur natif avec les informations de la base de données Mongodb de Wever.
				//req.session.nativeWeverUser = true;

				var usersex = "";

				if(result.sex == "femme")
				{
					usersex = "female";
				}
				if(result.sex == "homme")
				{
					usersex = "male";
				}

				var postalCode = null;

				if(typeof(result.adresses) != 'undefined' && result.adresses[0].postalCode)
				{
					postalCode = result.adresses[0].postalCode;
				}

				req.session.user = {"lastname" : result.lastname,"firstname" : result.firstname,"postalcode" : postalCode,"phone" : "0"+result.phone.slice(3),
						"gender":usersex,"birth":result.dob,"email" : result.email};
				req.session.notAllowedYet = true;

				// On lui envoie le code de connexion.
				var code = generateCode();
				sendCodeInSMS(code,"0"+result.phone.slice(3));
				savePhoneAndCode(phone,code);
				res.send("found");
			}
			/*else if(result != null && result.diagUser == "true") // Si utilisateur non natif...
			{
				var usersex = "";

				if(result.sex == "femme")
				{
					usersex = "female";
				}
				if(result.sex == "homme")
				{
					usersex = "male";
				}

				var postalCode = null;

				if(typeof(result.adresses) != 'undefined' && result.adresses[0].postalCode)
				{
					postalCode = result.adresses[0].postalCode;
				}

				// !! Valeurs de session temporaires. Ecrasées si l'utilisateur entre le bon code qui lui a été envoyé.
				req.session.user = {"lastname" : result.lastname,"firstname" : result.firstname,"postalcode" : postalCode,"phone" : "0"+result.phone.slice(3),
						"gender":usersex,"birth":result.dob,"email" : result.email};
				req.session.notAllowedYet = true;

				// On lui envoie le code de connexion.
				var code = generateCode();
				sendCodeInSMS(code,"0"+result.phone.slice(3));
				savePhoneAndCode(phone,code);
				res.send("found");
			}*/
			else if(result == null) // Si utilisateur non connu du système
			{
				res.send("unknown");
			}
		});
	}
})

.get("/weverAdresses",function(req,res){
	var locationSearchTerm = querystring.parse(url.parse(req.url).query)["locationSearchTerm"];
	request.get('https://apipp.wever.fr/widdle/wever/api/1.0/autocomplete?address='+locationSearchTerm+'&country=fr',function(err,httpResponse,result){
		if(typeof(result) != 'undefined')
		{
			res.send(JSON.stringify(JSON.parse(result)));
		}
		else
		{
			res.send(JSON.stringify(JSON.parse([])));
		}
	});
})

// Cette url renvoie les locations de Wever ressemblant à une string passée en paramètre
.get("/weverLocationsMatching",function(req,res){
	var weverLocationsMatching = {};
	var locationSearchTerm = querystring.parse(url.parse(req.url).query)["locationSearchTerm"];
	var regExp = new RegExp(""+locationSearchTerm,"i");
	var matchingCommunities = [];
	
	weverDB.collection("communities").find({"name": regExp}).toArray(function (error, results) {
        if (error) console.log(error);

        var lon;
        var lat;

        results.forEach(function(community) {
            if(community.stops[0]) {
            	if(community.stops[0].location.coordinates[0] && community.stops[0].location.coordinates[1])
            	{
            		lon = community.stops[0].location.coordinates[0];
            		lat = community.stops[0].location.coordinates[1];
            		matchingCommunities.push({"name" : community.name,"location" : { "lon":lon,"lat":lat }});
            	}
            }
            if(matchingCommunities.length == maxLocations) res.send(JSON.stringify(matchingCommunities));
        });

        if(matchingCommunities.length < maxLocations)
        {
        	res.send(JSON.stringify(matchingCommunities));
        }

    });
})

// Cette url gère la déconnexion
.get("/disconnect",function(req,res){
	req.session["user"] = null;
	req.session["notAllowedYet"] = null;
	//req.session["nativeWeverUser"] = null;
	res.redirect("/");
})

/**********************************************
	(Debut) Routes dédiées au module diags 
***********************************************/

/*
	( Début ) Creation et suppression de possibles valeurs de critères de type discret.
*/

// Url en POST permettant de créer une possible valeur pour les critères de type discrete.
.post("/createPossDiscCritVal",urlencodedParser,function(req,res){
	diags.createCriteriaAdmissibleValue(req.body.valCrit,function(){
		res.redirect("/admin");
	});
})

// Url en GET permettant de supprimer une possible valeur pour les critères de type discrete.
.get("/deletePossDiscCritVal/:id",function(req,res){
	diags.deleteCriteriaAdmissibleValueById(req.params.id,function(){
		res.redirect("/admin");
	});
})

/*
	( Fin ) Creation et suppression de valeurs possible de critères de type discret.
*/

/*
	( Début ) Creation et suppression de critères.
	Idem que précédemment.
*/

.post("/createCrit",urlencodedParser,function(req,res){
	if(typeof(req.body.admissibleValues) != 'undefined')
	{
		diags.createCriterion(req.body.type,req.body.name,req.body.valueType,req.body.admissibleValues,function(){
			res.redirect("/admin");
		});
	}
	else
	{
		diags.createCriterion(req.body.type,req.body.name,req.body.valueType,"",function(){
			res.redirect("/admin");
		});
	}
})

.get("/deleteCrit/:name",function(req,res){
	diags.deleteCriterion(req.params.name,function(){
		res.redirect("/admin");
	});
})

/*
	( Fin ) Creation et suppression de critères.
*/

// Uniquement pour les critères unimodaux.
// Permet de récupérer les diagnostiques concernant un départ ou une arrivée présents dans la base de données
// autour d'un point repéré par une latitude et une longitude (présentes dans l'url donnée).
// Voir le commentaire suivant sur la route /createDiag pour mieux appréhender la structure des diagnostics.
.get("/nearPoints",function(req,res){
	var queries = querystring.parse(url.parse(req.url).query);
	diags.nearPoints(parseFloat(queries['lat']),parseFloat(queries['lon']),m_maxRadius,function(matchingPoints){
		res.status(200).send(JSON.stringify(matchingPoints));
	});
})

.get("/nearTrips",function(req,res){
	var queries = querystring.parse(url.parse(req.url).query);
	console.log("RRRRRRRRRRRRRRRRRRRRRRRRRRRRRR");
	console.log(queries['latDeparture']);
	console.log(queries['lonDeparture']);
	console.log("RRRRRRRRRRRRRRRRRRRRRRRRRRRRRR");
	diags.nearTrips(parseFloat(queries['latDeparture']),parseFloat(queries['lonDeparture']),parseFloat(queries['latArrival']),parseFloat(queries['lonArrival']),m_maxRadius,m_maxRadius,function(matchingPoints){
		res.status(200).send(JSON.stringify(matchingPoints));
	});
})

// Cette route en méthode GET permet de gérer la création de diagnostiques.
// Comme on peut le voir, un diagnostique est en fait ... trois diagnostics.
// Un pour le départ, un pour le trajet et un pour l'arrivée.
.get("/createDiag",function(req,res){
	// On récupère et on désérialise les objets JSON.
	var diagDeparture = JSON.parse(querystring.parse(url.parse(req.url).query)['diagDeparture']);
	var diagTrip 	  = JSON.parse(querystring.parse(url.parse(req.url).query)['diagTrip']);
	var diagArrival   = JSON.parse(querystring.parse(url.parse(req.url).query)['diagArrival']);

	// On crée ensuite les diagnostique de départ, de trajet et d'arrivée.
	console.log("req.session['user'].phone");
	console.log(req.session["user"].phone);
	diags.createDiag(req.session["user"].phone,diagDeparture.lat1,diagDeparture.lon1,diagDeparture.lat2,diagDeparture.lon2,diagDeparture.criteria,diagDeparture.tripInformations);
	diags.createDiag(req.session["user"].phone,diagTrip.lat1,diagTrip.lon1,diagTrip.lat2,diagTrip.lon2,diagTrip.criteria,diagTrip.tripInformations);
	diags.createDiag(req.session["user"].phone,diagArrival.lat1,diagArrival.lon1,diagArrival.lat2,diagArrival.lon2,diagArrival.criteria,diagArrival.tripInformations);

	// On envoie le message received plutôt le message ok ou allright ou autres ... pour bien signifier que l'on a bien reçu
	// la demande, mais que l'on a pas l'assurance que tout ait été fini de traiter à cause du caractère asynchrone
	// de Node JS...
	// mais c'est de la paranoïa , normalement tout va assez vite.
	res.status(200).send("received");

})

/**********************************************
	(Fin) Routes dédiées au module diags 
***********************************************/

// Il faut réécrire le code de cette url. Pas du tout optimsé (le code est lent et le nombre de 
// requêtes utilisées est vraiment sous-optimal, mais ce code fonctionne au moins). Je décide du
// coup de commenter cette partie avant la mise en ligne et de ne la mettre en ligne que lorsque ce code sera
// parfaitement optimal (c'est important, trop de requêtes superflues vers les bases de données sont utilisées).
// Cette URL (protégée comme la page d'admin) permet de récupérer les résultats
// du diagnostic sous forme de fichier CSV. (C'est même peut-être l'URL la plus importante du site) et
// il est donc normal qu'elle soit protégée.
/*.get("/adminTripDatasCsvFormat",function(req,res){
	if( typeof(req.session.user) != 'undefined' && req.session['user'] != null && ( typeof(req.session.notAllowedYet) != 'undefined' && req.session.notAllowedYet == false ))
	{
		if(req.session["user"].phone == '0625613079')
		{
			var dataToCsvFormat = {};
			var sql = 'SELECT * from trips INNER JOIN diags ON trips.phoneUser = diags.phoneUser';
			connection.query(sql,function(err,row){
				var indexPhoneCorrelation = {};
				var cpt = 0;
				for(var i=0;i<row.length;i++)
				{

					if(typeof(indexPhoneCorrelation["+33"+row[i].phoneUser.slice(1)]) == 'undefined')
					{
						indexPhoneCorrelation["+33"+row[i].phoneUser.slice(1)] = [];
						indexPhoneCorrelation["+33"+row[i].phoneUser.slice(1)].push(i);
					}
					else
					{
						indexPhoneCorrelation["+33"+row[i].phoneUser.slice(1)].push(i);
					}
					
					// recherche des informations pas optimisé. A optimiser
					weverDB.collection("users").findOne({phone : "+33"+row[i].phoneUser.slice(1)}, function(error, result) {

						cpt++;

						var usersex = "";

						if(result.sex == "femme")
						{
							usersex = "female";
						}
						if(result.sex == "homme")
						{
							usersex = "male";
						}

						var postalCode = null;

						if(typeof(result.adresses) != 'undefined' && result.adresses[0].postalCode)
						{
							postalCode = result.adresses[0].postalCode;
						}

						indexPhoneCorrelation[result.phone].forEach(function(element) {
							row[element].lastname = result.lastname;
							row[element].firstname = result.firstname;
							row[element].postalCode = postalCode;
							row[element].phone = result.phone;
							row[element].gender = usersex;
							row[element].birth = result.dob;
							row[element].email = result.email;
						});

						if(cpt == row.length-1 && cpt>0)
						{
							dataToCsvFormat = row;
							res.send(json2csv({data : dataToCsvFormat, fields:Object.keys(row[0]) }));
						}
					});
				}
			});
		}
		else
		{
			res.redirect("/");
		}
	}
	else
	{
		res.redirect("/");
	}
})*/

// Route permettant d'accéder à la page d'admin permettant d'ajouter ou de modifier des critères.
// Le codeur de l'outil de diagnostique présent est le seul à y avoir accès.

.get("/admin",function(req,res){
	ctx = {};
	ctx["allCriteriaAdmissibleValues"] = diags.getAllCriteriaAdmissibleValues();
	ctx["allCriteria"] = diags.getCriteria();

	// On récupère la session de l'utilisateur
	ctx["user"] = req.session['user'];
	ctx["admin"] = false;
	if( typeof(req.session.user) != 'undefined' && req.session['user'] != null && ( typeof(req.session.notAllowedYet) != 'undefined' && req.session.notAllowedYet == false ))
	{
		if(req.session["user"].phone == '0625613079')
		{
			ctx["admin"] = true;
			res.render("diagsDemo.ejs",ctx);
		}
		else
		{
			res.redirect("/");
		}
	}
	else
	{
		res.redirect("/");
	}
})

// On gère les urls non définies en retournant une erreur 404.
.use(function(req,res,next){
	res.setHeader("Content-Type","text/html");
	res.status(404).send("<h1>Page introuvable !!</h1>");
});

if(process.env.NODE_ENV != 'production' || settings.server.mode == 'proxyfied')
{
	app.listen(process.env.SERVER_PORT || 8080);
}
else
{
	// Vous pourrez changer le port en production ici.
        var https = require('https');
        var fs = require('fs');
        var opt = {
          key: fs.readFileSync('certs/wever.fr.key'),
          cert: fs.readFileSync('certs/wever.chained.cer')
        };
        https.createServer(opt, app).listen(process.env.SERVER_PORT || 443);
}

// Cette fonction permet d' enregistrer un numéro de téléphone et un code dans la table connectionCodes.
function savePhoneAndCode(phone,code)
{
	var sql = 'INSERT INTO connectionCodes(phone,code) VALUES('+'"'+phone
				+'"'+","+'"'+code+'"'+')';
	connection.query(sql,function(err){});
}


// Cette fonction permet d' envoyer un code par SMS.
// Elle utilise l'API AllMySms. Une requête POST sur
// cette API permet de déclencher l'envoi de SMS.
function sendCodeInSMS(code,phone)
{
	var data = {
		mobile : phone,
		message : "Votre code d'accès est "+code+" .Bon diagnostic !",
		tpoa: 'Wever'
	};

	data = _.defaults(data, settings.allmysms.credentials);
	data = querystring.stringify(data);

	request.post(settings.allmysms.endpoint+data, function(error, response, body){
		if(error) console.log(error);
	});
	//return;
}

// Cette fonction génère un code de six chiffres de manière aléatoire.
// P.S : La fonction Math.random() ne renvoie jamais 1, mais peut renvoyer zéro. Ce qui
// nous assure bien un code sur six chiffres.
function generateCode()
{
	return ""+Math.floor( ( 100000 + Math.random()*900000 ) );
	//return "123456";
}