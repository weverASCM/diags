// Objet connexion pour accéder à la base de données MySQL depuis notre module diag.
var m_connection;

// Contiendra tous les valeurs admissibles pour les critères de type discrete.
var m_allCriteriaAdmissibleValues = {};

// Contiendra tous les valeurs admissibles pour les critères de type discrete.
var m_allCriteria = {};

// La valeur d'un degré en kilomètres.
var m_oneDegreeInKm = (40075.017/360);

// Dans une base de données SQL, pour récupérer des flottants, il faut définir 
// un certain degré de précision, le signe = ne marche pas. 
// La précision ci-dessous indiquée est la précision que nous tolérerons en ce qui concerne
// les coordonnées GPS stockées dans la base de données.
// Il faut savoir que un degré de position (latitude ou longitude) fait environ 78 km.
// Notre degré de précision ici tout calcul fait correspond donc à moins d'un décimètre.
// Ce qui nous semble acceptable pour gérer la précision sur les coordonnées GPS.
var m_gpsDegreePrecision = 0.000001;

// On récupère un pointeur de connexion vers la base de données.
var getConnectionPointer = function(connection,database) {
	m_connection = connection;
	m_connection.db = database;
}

// On insère une valeur admissible de critère toujours en minuscule.
var createCriteriaAdmissibleValue = function(criteriaAdmissibleValueName,callback)
{
	criteriaAdmissibleValueName = criteriaAdmissibleValueName.toLowerCase();
	var sql = 'SELECT * FROM criteriaAllAdmissibleValues WHERE value='+'"'+criteriaAdmissibleValueName+'"';
	m_connection.query(sql,function(err,row){
		if(err) return 0;
		if(row.length == 0)
		{
			var sql2 = 'INSERT INTO criteriaAllAdmissibleValues(value) VALUES('+'"'+criteriaAdmissibleValueName+'"'+')';
			m_connection.query(sql2,function(err){
				var sql3 = ' SELECT * FROM criteriaAllAdmissibleValues ORDER BY id DESC LIMIT 1;';
				m_connection.query(sql3,function(err,row){
					if(!err)
					{
						m_allCriteriaAdmissibleValues[row[0].id] = {"id" : row[0].id,"value" : row[0].value};
						callback();
					}
				});
			});
		}
		else
		{
			console.log("Le critere "+criteriaAdmissibleValueName+" est deja present");
		}
	});
}

// On récupère la liste des valeurs admissibles de critère de la base de données et on la stocke dans l' attribut
// privé m_allCriteriaAdmissibleValues.
var retrieveAllCriteriaAdmissibleValues = function()
{
	var sql = 'SELECT * FROM criteriaAllAdmissibleValues';
	
	m_connection.query(sql,function(err,row){
		if(err) return 0;
		for(var i=0;i<row.length;i++)
		{
			m_allCriteriaAdmissibleValues[row[i].id] = row[i] ;
		}
	});
}

var getAllCriteriaAdmissibleValues = function()
{
	return m_allCriteriaAdmissibleValues;
}

// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! //
/* Avant de supprimer une valeur de critère admissible vérifier que cette valeur n'est pas utilisée par un critère */

// On supprime une valeur admissible de critère de la base de donnees grâce a son id.
var deleteCriteriaAdmissibleValueById = function(criteriaAdmissibleValueId,callback)
{
	if(typeof(m_allCriteriaAdmissibleValues[criteriaAdmissibleValueId]) != 'undefined')
	{
		m_allCriteriaAdmissibleValues[criteriaAdmissibleValueId] = null;
		callback();
		var sql = 'DELETE FROM criteriaAllAdmissibleValues WHERE id='+'"'+criteriaAdmissibleValueId+'"';
		m_connection.query(sql,function(err){});
	}
}

// On supprime une valeur admissible de critère grâce à son nom.
// Cette fonction est obsolète pour le moment et pas du tout utilisée.
var deleteCriteriaAdmissibleValueByName = function(criteriaAdmissibleValueName)
{
	criteriaAdmissibleValueName = criteriaAdmissibleValueName.toLowerCase();
	var sql = 'DELETE FROM criteriaAllAdmissibleValues WHERE value='+'"'+criteriaAdmissibleValueName+'"';
	m_connection.query(sql,function(err){});
}

// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! //


var createCriterion = function(type,name,valueType,admissibleValuesString,callback)
{
		// On regarde si tous les ids présents dans la chaine admissibleValuesString
		// sont des ids de valeurs possibles de critères de type discrete présentes 

		var admissibleValues = admissibleValuesString.split(",");
		var admissibleValuesLength = admissibleValues.length;
		var cpt = 0;

		for(var i=0;i<admissibleValuesLength;i++)
		{
			if(typeof(m_allCriteriaAdmissibleValues[admissibleValues[i]]) != 'undefined' )
			{
				cpt++;
			}
		}

		// Le fait qu' il y ait ici deux conditions est dû au fait que split renvoie
		// un tableau de taille 1 pour une chaîne vide.
		if(admissibleValuesLength == cpt || (admissibleValuesString.length == 0)) // Si tous les ids sont corrects, on poursuit l'insertion
		{
			name = name.toLowerCase();
			// on recherche si le nom du critère en miniscule existe déjà dans la base de données.
			var sql = 'SELECT * FROM criteria WHERE criterionName = "'+name+'"';
			m_connection.query(sql,function(err,row){
				if(row.length == 0) // Si le critère n' existe pas déja
				{
					// On crée le critère dans la table critère
					var sql2 = 'INSERT INTO criteria(criterionType,criterionName,criterionValueType,criterionAdmissibleValues) VALUES('+'"'+type
								+'"'+","+'"'+name+'"'+","+'"'+valueType+'"'+","+'"'+admissibleValuesString+'"'+')';
					m_connection.query(sql2,function(err){});

					// On le rajoute comme colonne dans la table diags
					var sql3 = 'ALTER TABLE diags ADD COLUMN '+name+' VARCHAR(255)';
					m_connection.query(sql3,function(err){});

					var sql3 = ' SELECT * FROM criteria ORDER BY id DESC LIMIT 1;';
					m_connection.query(sql3,function(err,row){
						if(!err)
						{
							m_allCriteria[row[0].id] = {"id" : row[0].id,"criterionType" : row[0].criterionType,
							"criterionName":row[0].criterionName,"criterionValueType":row[0].criterionValueType,
							"criterionAdmissibleValues":row[0].criterionAdmissibleValues};
							callback();
						}
					});
				}
			});
		}
		else // Sinon on met fin en retournant 0.
		{
			return 0;
		}
}

var deleteCriterion = function(name,callback)
{
	name = name.toLowerCase();

	var sql3 = "SELECT column_name FROM information_schema.columns WHERE table_name = 'diags' AND table_schema='"+m_connection.db+"' AND column_name='"+name+"'";
	m_connection.query(sql3,function(err,row){
		if(row.length > 0)
		{
			for(criterionId in m_allCriteria)
			{
				if(m_allCriteria[criterionId] != null)
				{
					if( m_allCriteria[criterionId].criterionName == name)
					{
						 m_allCriteria[criterionId] = null;
						 callback();
					}
				}
			}
			var sql = 'DELETE FROM criteria WHERE criterionName='+'"'+name+'"';
			m_connection.query(sql,function(err){
				if(!err)
				{
					var sql2 = 'ALTER TABLE diags DROP COLUMN '+name;
					m_connection.query(sql2,function(err){});
				}
			});
		}
	});
}

var retrieveCriteria = function()
{
	var sql = 'SELECT * FROM criteria';
	
	m_connection.query(sql,function(err,row){
		if(err) return 0;
		for(var i=0;i<row.length;i++)
		{
			m_allCriteria[row[i].id] = row[i] ;
		}
	});
}

var getCriteria = function()
{
	return m_allCriteria;
}

var createDiag = function(phoneUser,lat1,lon1,lat2,lon2,criteria,tripInformations)
{
	console.log("Coordonnees geographiques : Debut");
	console.log(lat1);
	console.log(lon1);
	console.log(lat2);
	console.log(lon2);
	console.log("Coordonnees geographiques : Fin");
	// Vérifier que tous les critères spécifiés sont bien présents dans la table Diag, sinon on annule l'insertion
	var sql = "SELECT column_name FROM information_schema.columns WHERE table_name = 'diags' AND table_schema='"+m_connection.db+"'";
	m_connection.query(sql,function(err,row){

		var criterionComponents = null;
		var critExists = false;

		var regExp = null;

		// Boucle servant à vérifier la validité de tous les critères.
		for(criterion in criteria)
		{
			console.log("criterion : "+criterion);

			for(var j=6;j<row.length;j++) // On vérifie si le critère est dans la table diags.
			{
				if(row[j].column_name == criterion)
				{
					critExists = true;
				}
			}

			// Si le critère n' existe pas dans la table diags, on stoppe le processus.
			if(!critExists) return 0;

			critExists = false;

			// Si un seul des critères a une valeur vide...
			// On arrête l'insertion car il faut au moins savoir
			// si c'est sur le départ ou l' arrivée ou les deux 
			// que le critère s' applique.
			if((criteria[criterion].value).length == 0) return 0;

			criterionComponents = (criteria[criterion].value).split(':');

			console.log("criterionComponents : "+criterionComponents.length);

			console.log("1");
			// Le paramètre qui permet de savoir si le critère est sur l'arrivée ou le départ doît être positif et inférieur ou égal à trois.
			// Il est codé comme suit : départ (0|1) , arrivé (0|1). Donc...
			// 00 => ni départ, ni arrivée (non renseigné) = 0 si on passe du binaire en décimal.
			// 01 => renseigné sur l'arrivée, = 1.
			// 10 => renseigné sur le départ, = 2.
			// 11 => renseigné sur le départ et l'arrivée, = 3.
			if( parseInt(criterionComponents[0],10) < 0 || parseInt(criterionComponents[0],10) > 3) return 0;

			console.log("2");

			if(criterionComponents.length > 1)
			{
				console.log("2-1");
				// Si le critère est discret, la seconde valeur doit être une valeur admissible.
				if(m_allCriteria[criteria[criterion].id].criterionValueType == 'discrete')
				{
					console.log("2-2");
					console.log("criterionComponents[1] : "+criterionComponents[1]);
					regExp = new RegExp(""+criterionComponents[1],"i");
					if(!regExp.test(m_allCriteria[criteria[criterion].id].criterionAdmissibleValues)) return 0;
				}
				console.log("2-3");
			}
			if(criterionComponents.length > 2) // Ici, nous gèrerons les commentaires possibles. Mais cette fonctionnalité n'est pas opérationnelle.
			{

			}
			if(criterionComponents.length > 3) return 0; // La chaîne de renseignement des valeurs doit avoir au maximum trois champs significatifs.
			console.log("3");
		}
		console.log("Toutes les étapes ont ete passees avec succes");

		var sql2 = 'SELECT * FROM trips WHERE abs(lat1-'+parseFloat(lat1)+') < '+m_gpsDegreePrecision+' AND abs(lon1-'+
			parseFloat(lon1)+') < '+m_gpsDegreePrecision+' AND abs(lat2-'
			+parseFloat(lat2)+') < '+m_gpsDegreePrecision+' AND abs(lon2-'+parseFloat(lon2)+') < '+m_gpsDegreePrecision+' AND phoneUser="'+phoneUser+'"';
		console.log("sql2");
		console.log(sql2);
		m_connection.query(sql2,function(err,row){
			if(err) return 0;

			if(row.length == 0)
			{
				var sql3 = 'INSERT INTO trips(phoneUser,lat1,lon1,lat2,lon2,mode,departure,departureHour,arrival,arrivalHour,AR) VALUES("'+phoneUser+'",'+parseFloat(lat1)+','+parseFloat(lon1)+','+
					parseFloat(lat2)+','+parseFloat(lon2)+','+'"'+tripInformations.mode+'"'+','+'"'+tripInformations.departure+'"'+','+'"'+tripInformations.departureHour+'"'+
					','+'"'+tripInformations.arrival+'"'+','+'"'+tripInformations.arrivalHour+'"'+','+parseInt(tripInformations.AR)+')';
				m_connection.query(sql3,function(err){if(err) console.log(err);});

				var sql = 'INSERT INTO diags(phoneUser,lat1,lon1,lat2,lon2';
				for(criterion in criteria)
				{
					sql = sql + ',' + criterion ;
				}
				sql = sql+') VALUES("'+phoneUser+'",'+parseFloat(lat1)+','+parseFloat(lon1)+','+parseFloat(lat2)+','+parseFloat(lon2);
				for(criterion in criteria)
				{
					sql = sql + ','+'"'+criteria[criterion].value+'"' ;
				}
				sql = sql + ')';

				m_connection.query(sql,function(err){if(err) console.log(err);});
			}
			else if(row.length > 0)
			{
				console.log("coordonnees deja presentes.");
				var sql4 = 'UPDATE diags SET ';
				for(criterion in criteria)
				{
					sql4 = sql4 + criterion  + ' = "' + criteria[criterion].value + '", ';
				}
				sql4 = sql4.substr(0,sql4.length-2);
				sql4 = sql4 + ' WHERE abs(lat1-'+parseFloat(lat1)+') < '+m_gpsDegreePrecision+' AND abs(lon1-'+parseFloat(lon1)
					+') < '+m_gpsDegreePrecision+' AND abs(lat2-'+parseFloat(lat2)
					+') < '+m_gpsDegreePrecision+' AND abs(lon2-'+parseFloat(lon2)+') < '+m_gpsDegreePrecision+' AND phoneUser = "'+phoneUser+'"';
				console.log("sql4");
				console.log(sql4);
				m_connection.query(sql4,function(err){if(err) console.log(err);});
			}
		});
	});
}

var min = function(x,y)
{
	if(x <= y) return x;
	return y;
}

var max = function(x,y)
{
	if(x >= y) return x;
	return y;
}

function deg2rad(deg) {
	return deg * (Math.PI / 180);
}

var distance = function(lat1,lon1,lat2,lon2)
{
	var R = 6378.137; // Radius of the earth in km
    var dLat = deg2rad(lat2 - lat1); // deg2rad below
    var dLon = deg2rad(lon2 - lon1);
    var a =
	Math.sin(dLat / 2) * Math.sin(dLat / 2) +
	Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) *
	Math.sin(dLon / 2) * Math.sin(dLon / 2);
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    return R * c;
}

// Récupère les points qui sont dans le cercle de centre le point terrestre
// de latitude lat et longitude lon et de rayon radiusInKm (le rayon est en km)
// Cette fonction est utilisée pour traiter le diag dans le cas de critère unimodaux.
var nearPoints = function(lat,lon,radiusInKm,callback)
{
    var radiusInDegree = (radiusInKm / m_oneDegreeInKm);
    var lowerLat = max(lat - radiusInDegree,(-1)*90);
    var greaterLat = min(lat + radiusInDegree,90);
    var lowerLon = max(lon - radiusInDegree,(-1)*180);
    var greaterLon = min(lon + radiusInDegree,180);

    // On sélectionne de la base de données les points qui se trouvent dans le carré de longueur 2*radiusInKm et de centre lat,lon
    var sql = 'SELECT * FROM diags WHERE (lat1 >= '+lowerLat+' AND lat1 <= '+greaterLat+' AND lon1 >= '+
    lowerLon+' AND lon1 <= '+greaterLon+') || (lat2 >= '+lowerLat+' AND lat2 <= '+greaterLat+' AND lon2 >= '+lowerLon+' AND lon2 <= '+
    greaterLon+')';
    m_connection.query(sql,function(err,row){
  	if(err) return 0;

  	var matchingPoints = {};
  	var dist = 0;

  	for(var i=0;i < row.length;i++)
  	{

  		// Les deux conditions if permettent d'exclure les diags concernant le trajet.
  		// On est ici dans le cas de critères unimodaux et on ne sélectionne que des
  		// départs et des arrivées.

  		if(parseFloat(row[i].lat1) != 181 && parseFloat(row[i].lat2) == 181)
  		{
  			dist = distance(lat,lon,row[i].lat1,row[i].lon1);

  			// On n'oublie pas que un carré , ce n'est pas un cercle !
  			if(dist <= radiusInKm)
	  		{
	  			matchingPoints[row[i].id] = row[i];
	  		}
  		}
  		else if(parseFloat(row[i].lat1) == 181 && parseFloat(row[i].lat2) != 181)
  		{
  			dist = distance(lat,lon,row[i].lat2,row[i].lon2);

  			// Idem que la condition if précédente.
  			if(dist <= radiusInKm)
	  		{
	  			matchingPoints[row[i].id] = row[i];
	  		}
  		}
  	}

  	callback(matchingPoints);

    });
}

var nearTrips = function(latDeparture,lonDeparture,latArrival,lonArrival,departureRadiusInKm,arrivalRadiusInKm,callback)
{ 	  
	var departureRadiusInDegree = (departureRadiusInKm / m_oneDegreeInKm);
	var arrivalRadiusInDegree = (arrivalRadiusInKm / m_oneDegreeInKm);

	var lowerLatDeparture = max(latDeparture - departureRadiusInDegree,(-1)*90);
	var lowerLonDeparture = max(lonDeparture - departureRadiusInDegree,(-1)*180);
	var greaterLatDeparture = min(latDeparture + departureRadiusInDegree,90);
	var greaterLonDeparture = min(lonDeparture + departureRadiusInDegree,180);

	var lowerLatArrival = max(latArrival - arrivalRadiusInDegree,(-1)*90);
	var lowerLonArrival = max(lonArrival - arrivalRadiusInDegree,(-1)*180);
	var greaterLatArrival = min(latArrival + arrivalRadiusInDegree,90);
	var greaterLonArrival = min(lonArrival + arrivalRadiusInDegree,180);

	var sql = 'SELECT * FROM diags WHERE (lat1 >= '+lowerLatDeparture+' AND lat1 <= '+greaterLatDeparture+' AND lon1 >= '+
	lowerLonDeparture+' AND lon1 <= '+greaterLonDeparture+') && (lat2 >= '+lowerLatArrival+' AND lat2 <= '+greaterLatArrival+' AND lon2 >= '+lowerLonArrival+' AND lon2 <= '+
	greaterLonArrival+')';

	m_connection.query(sql,function(err,row){
		if(err) return 0;

		var matchingPoints = {};
		
		var dist1 = 0;
		var dist2 = 0;

		console.log("latDeparture : "+latDeparture);
		console.log("latArrival : "+latArrival);

		console.log("latDeparture : "+latDeparture);
		console.log("latArrival : "+latArrival);

		for(var i=0;i < row.length;i++)
		{
			dist1 = distance(latDeparture,lonDeparture,row[i].lat1,row[i].lon1);
			dist2 = distance(latArrival,lonArrival,row[i].lat2,row[i].lon2);

			// On n'oublie pas que un carré, ce n'est pas un cercle !
			if(dist1 <= departureRadiusInKm && dist2 <= arrivalRadiusInKm)
			{
				matchingPoints[row[i].id] = row[i];
			}
		}

		console.log("MatchingPoints");
		console.log(matchingPoints);
		callback(matchingPoints);

	});
}

// On définit les fonctions que nous voulons rendre publiques à partir de ce module.
exports.getConnectionPointer = getConnectionPointer;
exports.createCriteriaAdmissibleValue = createCriteriaAdmissibleValue;
exports.retrieveAllCriteriaAdmissibleValues = retrieveAllCriteriaAdmissibleValues;
exports.getAllCriteriaAdmissibleValues = getAllCriteriaAdmissibleValues;
exports.deleteCriteriaAdmissibleValueById = deleteCriteriaAdmissibleValueById;
exports.deleteCriteriaAdmissibleValueByName = deleteCriteriaAdmissibleValueByName;
exports.createCriterion = createCriterion;
exports.deleteCriterion = deleteCriterion;
exports.retrieveCriteria = retrieveCriteria;
exports.getCriteria = getCriteria;
exports.createDiag = createDiag;
exports.nearPoints = nearPoints;
exports.nearTrips = nearTrips;