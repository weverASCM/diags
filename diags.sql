-- MySQL dump 10.13  Distrib 5.5.54, for debian-linux-gnu (i686)
--
-- Host: localhost    Database: Diag
-- ------------------------------------------------------
-- Server version	5.5.54-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `Diag`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `Diag` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `Diag`;

--
-- Table structure for table `comments`
--

DROP TABLE IF EXISTS `comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comments` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `idUser` smallint(5) unsigned NOT NULL,
  `comment` text,
  `insertDate` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `connectionCodes`
--

DROP TABLE IF EXISTS `connectionCodes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `connectionCodes` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `phone` varchar(10) DEFAULT NULL,
  `code` varchar(6) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `criteria`
--

DROP TABLE IF EXISTS `criteria`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `criteria` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `criterionType` varchar(8) DEFAULT NULL,
  `criterionName` varchar(255) DEFAULT NULL,
  `criterionValueType` varchar(8) DEFAULT NULL,
  `criterionAdmissibleValues` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `criteriaAllAdmissibleValues`
--

DROP TABLE IF EXISTS `criteriaAllAdmissibleValues`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `criteriaAllAdmissibleValues` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `value` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `diags`
--

DROP TABLE IF EXISTS `diags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `diags` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `phoneUser` varchar(10) DEFAULT NULL,
  `lat1` float DEFAULT NULL,
  `lon1` float DEFAULT NULL,
  `lat2` float DEFAULT NULL,
  `lon2` float DEFAULT NULL,
  `transport` varchar(255) DEFAULT NULL,
  `confort` varchar(255) DEFAULT NULL,
  `parking` varchar(255) DEFAULT NULL,
  `horaire` varchar(255) DEFAULT NULL,
  `securite` varchar(255) DEFAULT NULL,
  `arret` varchar(255) DEFAULT NULL,
  `correspondance` varchar(255) DEFAULT NULL,
  `information` varchar(255) DEFAULT NULL,
  `complet` varchar(255) DEFAULT NULL,
  `permis` varchar(255) DEFAULT NULL,
  `voiture` varchar(255) DEFAULT NULL,
  `dureetrajet` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `test`
--

DROP TABLE IF EXISTS `test`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `test` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `text` text,
  `insert_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `trips`
--

DROP TABLE IF EXISTS `trips`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trips` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `phoneUser` varchar(10) DEFAULT NULL,
  `lat1` float DEFAULT NULL,
  `lon1` float DEFAULT NULL,
  `lat2` float DEFAULT NULL,
  `lon2` float DEFAULT NULL,
  `mode` varchar(50) DEFAULT NULL,
  `departure` varchar(500) DEFAULT NULL,
  `departureHour` varchar(10) DEFAULT NULL,
  `arrival` varchar(500) DEFAULT NULL,
  `arrivalHour` varchar(10) DEFAULT NULL,
  `AR` smallint(5) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-05-19 14:35:31
