# README #

* Site web permettant de diagnostiquer les problèmes de mobilité d'usagers (qu'ils soient ou non chez Wever)
* Version 0.0

# Comment installer l'application #

* La base de données

  La première des choses à faire est de créer la base de données.
  Connectez-vous à MySQL et créez la base de données (Appellez la comme vous voulez). 
  Ouvrez le fichier nodeTest.sql (un mysqldump de ce fichier n'est pas approprié).
  Copiez à la main chaque définition de table de ce fichier dans la commande MySQL.

# Comment exécuter l'application #

* Ouvrez le fichier server.js et indiquez le chemin des bases de données MySQL et Wever
  ainsi que les données d'authentification
* Lancez l'application avec node server.
